/* eslint-disable new-cap */
'use strict';

const express = require('express');
const routes = express.Router();
const session = require('express-session');
const authController = require('../controllers/accounts.controller');
const blogController = require('../controllers/contents.controller');

routes.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true},
));
routes.get('/', (req, res) => {
  const indexResult= {
    message: 'Kukkui Sertis Lab HNY',
  }; res.status(200).json(indexResult);
});
routes.use(express.json());
routes.post('/auth', authController.authorized);
routes.get('/posts/me', blogController.myPosts);
routes.post('/posts/new', blogController.addPosts);
routes.get('/posts/all', blogController.allPosts);
routes.put('/posts/edit/:id', blogController.editPosts);
routes.delete('/posts/delete/:id', blogController.deletePosts);

module.exports = routes;
