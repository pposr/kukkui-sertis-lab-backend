/* eslint-disable no-unused-vars */
/* eslint-disable require-jsdoc */
/* eslint-disable max-len */
'use strict';

const accountsMongooseModel = require('../models/accounts.model');
const accountsHelperFunction = require('../helpers/accounts.helper');

exports.authorized = async (req, res, err) => {
  const accFromSession = req.session;
  const {username: reqBodyUsername, password: reqBodyPassword} = req.body;

  try {
    const query = {username: reqBodyUsername};
    const accFromRecord = await accountsMongooseModel.findOne(query);
    const getObj = {accFromRecord, reqBodyUsername, reqBodyPassword};

    if (accFromRecord) {
      const accMatched = await accountsHelperFunction.checkPassword(getObj);
      accFromSession.username = reqBodyUsername;
      accFromSession.password = reqBodyPassword;
      return res.status(200).json({'AUTHORIZATION STATUS': accMatched});
    } else {
      const result = await accountsHelperFunction.createNewAccount(getObj);
      return res.status(201).json({'NEW ACCOUNT GENERATED': result});
    }
  } catch (error) {
    return res.status(500).json({error});
  }
};
