/* eslint-disable no-invalid-this */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
'use strict';

const accountsMongooseModel = require('../models/accounts.model');
const contentsMongooseModel = require('../models/contents.model');
const contentsHelperFunction = require('../helpers/contents.helper');

exports.myPosts = async (req, res, next) => {
  try {
    const sendObj = {
      mode: 'view',
      currentSession: req.session,
      reqBodyUsername: req.body.username,
      reqBodyPassword: req.body.password,
      reqBody: req.body,
      paramsID: null,
    };
    const showResult = await this.postContents(sendObj);
    return res.status(200).json({'MY POSTS': showResult});
  } catch (error) {
    return res.status(500).json({error});
  }
};

exports.addPosts = async (req, res, next) => {
  try {
    const sendObj = {
      mode: 'add',
      currentSession: req.session,
      reqBodyUsername: req.body.username,
      reqBodyPassword: req.body.password,
      reqBody: req.body,
      paramsID: null,
    };
    const showResult = await this.postContents(sendObj);
    return res.status(201).json({'COMPLETED ADD': showResult});
  } catch (error) {
    return res.status(500).json({error});
  }
};

exports.allPosts = async (req, res, next) => {
  try {
    const result = await contentsMongooseModel.find({});
    return res.status(200).json({'ALL POSTS': result});
  } catch (error) {
    return res.status(500).json({error,
    });
  }
};

exports.editPosts = async (req, res, next) => {
  try {
    const sendObj = {
      mode: 'edit',
      currentSession: req.session,
      reqBodyUsername: req.body.username,
      reqBodyPassword: req.body.password,
      reqBody: req.body,
      paramsID: req.params.id,
    };
    const showResult = await this.postContents(sendObj);
    return res.status(200).json({'COMPLETED EDIT': showResult});
  } catch (error) {
    return res.status(500).json({error});
  }
};

exports.deletePosts = async (req, res, next) => {
  try {
    const sendObj = {
      mode: 'delete',
      currentSession: req.session,
      reqBodyUsername: req.body.username,
      reqBodyPassword: req.body.password,
      reqBody: req.body,
      paramsID: req.params.id,
    };
    const showResult = await this.postContents(sendObj);
    return res.status(200).json({'COMPLETED DELETE': showResult});
  } catch (error) {
    return res.status(500).json({error});
  }
};

exports.postContents = async function({mode, currentSession, reqBodyUsername, reqBodyPassword, reqBody, paramsID}) {
  try {
    const accFromSession= await contentsHelperFunction.checkSessionExist({
      accFromSession: currentSession,
      reqBodyUsername,
      reqBodyPassword,
    });
    const accFromRecord = await accountsMongooseModel.findOne({username: accFromSession.username});
    const accMatched = await contentsHelperFunction.usernameCheck({accFromRecord, accFromSession, reqBodyUsername, reqBodyPassword});
    const sendObj = {
      mode,
      accFromSession,
      reqBodyUsername,
      reqBodyPassword,
      reqBody,
      paramsID,
      accFromRecord,
      accMatched,
    };
    const showResult = await contentsHelperFunction.finalResultFromMode(sendObj);
    if (sendObj.accMatched) return showResult;
    else return [];
  } catch (error) {
    throw error;
  }
};
