'use strict';

const bcrypt = require('bcryptjs');

exports.preSaveHook = async function(next, anySchema) {
  const checkModified = anySchema.isModified('password');
  if (!checkModified) return next();
  anySchema.password = await bcrypt.hash(anySchema.password, 12);
  anySchema.passwordConfirm = undefined;
  return next();
};
