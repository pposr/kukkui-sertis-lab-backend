/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
'use strict';

const accountsMongooseModel = require('../models/accounts.model');
const generator = require('generate-password');

exports.checkPassword = async function({accFromRecord, reqBodyUsername, reqBodyPassword}) {
  try {
    const accMatched = await accFromRecord.correctPassword(reqBodyPassword, accFromRecord.password);
    return accMatched;
  } catch (error) {
    throw error;
  }
};

exports.createNewAccount = async function({reqBodyUsername}) {
  try {
    const generatedPassword = await generator.generate({length: 10, numbers: true});
    const query = {username: reqBodyUsername, password: generatedPassword};
    await accountsMongooseModel.create(query); // does this return something to use?
    return (query);
  } catch (error) {
    throw error;
  }
};
