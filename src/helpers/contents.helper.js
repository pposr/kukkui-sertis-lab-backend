/* eslint-disable new-cap */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
'use strict';

const contentsMongooseModel = require('../models/contents.model');
const contentsHelperFunction = require('./contents.helper');
const bcrypt = require('bcryptjs');

exports.checkSessionExist = async function({accFromSession, reqBodyUsername, reqBodyPassword}) {
  if (accFromSession.username && accFromSession.password) {
    reqBodyUsername = accFromSession.username;
    reqBodyPassword = accFromSession.password;
  }
  return {username: reqBodyUsername, password: reqBodyPassword};
};

exports.usernameCheck = async function({accFromRecord, accFromSession}) {
  try {
    return await bcrypt.compare(accFromSession.password, accFromRecord.password);
  } catch (error) {
    throw error;
  }
};

exports.viewModeResult = async function({accFromSession}) {
  try {
    if (accFromSession.username != undefined) {
      const query = {username: accFromSession.username};
      return await contentsMongooseModel.find(query);
    } return [];
  } catch (error) {
    throw error;
  }
};

exports.editModeResult = async function({reqBody, paramsID, accFromSession}) {
  try {
    const query= {_id: paramsID, username: accFromSession.username};
    const result = await contentsMongooseModel.findOneAndUpdate(query, reqBody);
    return (result);
  } catch (error) {
    throw error;
  }
};

exports.addModeResult = async function({reqBody}) {
  try {
    return await contentsMongooseModel.create(reqBody);
  } catch (error) {
    throw (error);
  }
};

exports.deleteModeResult = async function({paramsID, accFromSession}) {
  try {
    const query = {_id: paramsID, username: accFromSession.username};
    return await contentsMongooseModel.deleteOne(query);
  } catch (error) {
    throw error;
  }
};

exports.finalResultFromMode = async function(getObj) {
  try {
    const func={
      view: contentsHelperFunction.viewModeResult,
      add: contentsHelperFunction.addModeResult,
      edit: contentsHelperFunction.editModeResult,
      delete: contentsHelperFunction.deleteModeResult,
    };
    return await func[getObj.mode](getObj);
  } catch (error) {
    throw (error);
  }
};
