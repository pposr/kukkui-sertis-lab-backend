/* eslint-disable max-len */
'use strict';

const express = require('express');
const routes = require('./routes/index');
const bodyParser = require('body-parser');
const dbConfig = require('./configs/development.config.js');
const mongoose = require('mongoose');

const app = express();
app.use(express.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url, {
  useNewUrlParser: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
}).then(() => {
  console.log(JSON.stringify({
    msg: 'Successfully connected to the kukkui mongo database',
  }));
}).catch((err) => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

app.use('/', routes);
const port = process.env.PORT || 5000;
module.exports = app.listen(port, () => console.log(`Listening on port : ${port}...`) );
