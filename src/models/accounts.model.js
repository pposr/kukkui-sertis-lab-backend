/* eslint-disable new-cap */
/* eslint-disable max-len */
/* eslint-disable no-invalid-this */
'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const hookMongooseModel = require('../helpers/hook.helper');
const accountSchema = mongoose.Schema({
  username: String,
  password: String,
});

accountSchema.pre('save', async function(next) {
  return await hookMongooseModel.preSaveHook(next, this);
});

accountSchema.methods.correctPassword = async function(
    typedPassword,
    originalPassword,
) {
  return await bcrypt.compare(typedPassword, originalPassword);
};

const Auth = mongoose.model('Auth', accountSchema);
module.exports = Auth;
