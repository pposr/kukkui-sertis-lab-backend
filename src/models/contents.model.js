/* eslint-disable new-cap */
/* eslint-disable no-invalid-this */
'use strict';

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const hookMongooseModel = require('../helpers/hook.helper');

const contentSchema = mongoose.Schema({
  username: String,
  content: String,
  cardName: String,
  cardStatus: String,
  cardContent: String,
  cardCategory: String,
});

contentSchema.pre('save', async function(next) {
  return await hookMongooseModel.preSaveHook(next, this);
});

contentSchema.methods.correctPassword = async function(
    typedPassword,
    originalPassword,
) {
  return await bcrypt.compare(typedPassword, originalPassword);
};

const Blog = mongoose.model('Blog', contentSchema);
module.exports = Blog;
