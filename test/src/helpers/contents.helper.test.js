/* eslint-disable camelcase */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(4) contents.helpers.js : additional methods for contents.controller.js', () => {
  let contentsModelMock;
  let accFromSession;
  let checkSessionObj;
  let checkSessionResult;
  let reqBodyUsername;
  let reqBodyPassword;
  let comparePasswordStub;
  let paramsID;
  let reqBody;
  let req;
  let next;
  let func;
  let getObj;
  let post;
  let jsonStub;
  let contentsHelperMock;
  let objTest;
  let contentsHelper;
  let viewModeResult;
  let initContentsHelper;
  let showResult;
  let bcryptMock;
  let accFromRecord;

  beforeEach(() => {
    reqBody = {};
    bcryptMock = {};
    contentsModelMock = {};
    post = 'res.send.done';
    showResult = {message: 'hiResult'};
    bcryptMock = {
      compare: sinon.stub(),
    };
    contentsHelper = {
      checkSessionExist: sinon.stub(),
      usernameCheck: sinon.stub(),
      viewModeResult: sinon.stub(),
      editModeResult: sinon.stub(),
      addModeResult: sinon.stub(),
      deleteModeResult: sinon.stub(),
      finalResultFromMode: sinon.stub(),
    };
    contentsHelperMock = {
      finalResultFromMode: sinon.stub(),
      viewModeResult: sinon.stub(),
    };
    jsonStub = {message: 'json_test'},
    comparePasswordStub = sinon.stub();
    contentsModelMock = {
      create: sinon.stub(),
      find: sinon.stub(),
      findOneAndUpdate: sinon.stub(),
      deleteOne: sinon.stub(),
    };

    reqBody = {
      comparePassword: sinon.stub(),
    };
    req = {
      session: {
        username: 'test',
        password: 'test',
      },
      body: {
        username: 'test',
        password: 'test',
        content: 'new content',
        cardName: 'new cardname',
        cardStatus: 'new cardstatus',
        cardContent: 'new cardcontent',
        cardCategory: 'new cardcategory',
      },
      params: {
        id: {},
      },
    };
    accFromRecord = {
      username: 'test',
      password: 'test',
    };
    accFromSession = {
      username: 'test',
      password: 'test',
    };
    viewModeResult = sinon.stub();
    objTest =
            {
              _id: 0,
              username: 'test',
              content: 'test',
              cardName: 'test',
              cardStatus: 'test',
              cardContent: 'test',
              cardCategory: 'test',
              __v: 0,
            };

    contentsHelperMock = {
      viewModeResult: sinon.stub().returns({}),
    };
    func = {
      view: contentsHelperMock.viewModeResult,
      add: 'MOCK ADD',
      edit: 'MOCK EDIT',
      delete: 'MOCK DELETE',
    };
    getObj = {
      accMatched: {},
      mode: 'view',
      accFromSession: 'test',
      reqBodyUsername: 'test',
      reqBodyPassword: 'test',
      reqBody: 'test',
      paramsID: 'test',
    };
    accFromSession = {
      username: 'test2',
      password: 'test2',
    };
    reqBodyUsername = req.body.username;
    reqBodyPassword = req.body.password;
    reqBody = req.body;
    paramsID = req.params.id;
    next = sinon.stub();
    initContentsHelper = () => {
      contentsHelper = proxyquire('../../../src/helpers/contents.helper', {
        '../models/contents.model': contentsModelMock,
        '../helpers/contents.helper': contentsHelperMock,
        'bcryptjs': bcryptMock,
      });
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.checkSessionExist()', () => {
    it('should call .checkSessionExist() correctly with session defined', async () => {
      initContentsHelper();
      req.session.username = 'test2';
      req.session.password = 'test2';
      checkSessionObj = {accFromSession, reqBodyUsername, reqBodyPassword};
      checkSessionResult = await contentsHelper.checkSessionExist(checkSessionObj);
      assert.deepEqual(checkSessionResult, ({username: 'test2', password: 'test2'}));
    });

    it('should call .checkSessionExist() correctly with session undefined', async () => {
      initContentsHelper();
      accFromSession = {};
      checkSessionObj = {accFromSession, reqBodyUsername, reqBodyPassword};
      checkSessionResult = await contentsHelper.checkSessionExist(checkSessionObj);
      assert.deepEqual(checkSessionResult, {username: 'test', password: 'test'});
    });
  });

  describe('.usernameCheck()', () => {
    it('should call .usernameCheck() correctly with true condition', async () => {
      initContentsHelper();
      bcryptMock.compare = sinon.stub().resolves(true);
      assert.deepEqual(await contentsHelper.usernameCheck({accFromRecord, accFromSession}), true);
    });

    it('should call .usernameCheck() function with false condition', async () => {
      initContentsHelper();
      bcryptMock.compare.resolves(false);
      assert.deepEqual(await contentsHelper.usernameCheck({accFromRecord, accFromSession}), false);
    });

    it('should call .usernameCheck() incorrectly with throw error', async () => {
      initContentsHelper();
      const error = new Error('error');
      bcryptMock.compare.throws(error);
      contentsHelper.checkSessionExist = sinon.stub().throws(error);
      try {
        await contentsHelper.usernameCheck({accFromRecord, accFromSession});
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });

  describe('.viewModeResult()', () => {
    it('should call .viewModeResult() correctly with true condition', async () => {
      initContentsHelper();
      contentsModelMock.find = sinon.stub().resolves(accFromRecord);
      assert.deepEqual(await contentsHelper.viewModeResult({accFromSession}), accFromRecord);
    });

    it('should call .viewModeResult() correctly with false condition', async () => {
      initContentsHelper();
      accFromSession = {username: undefined, password: undefined};
      assert.deepEqual(await contentsHelper.viewModeResult({accFromSession}), []);
    });

    it('should call .viewModeResult() with throw error', async () => {
      initContentsHelper();
      const error = new Error('error');
      contentsModelMock.find = sinon.stub().throws(error);
      try {
        await contentsHelper.viewModeResult({accFromSession});
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });

  describe('.editModeResult()', () => {
    it('should call .editModeResult() correctly', async () => {
      initContentsHelper();
      req.params.id = 0;
      contentsModelMock.findOneAndUpdate = sinon.stub().resolves(reqBody);
      assert.deepEqual(await contentsHelper.editModeResult({reqBody, paramsID, accFromSession}), reqBody);
    });

    it('should call .editModeResult() incorrectly with throw error', async () => {
      initContentsHelper();
      req.params.id = 0;
      const error = new Error('error');
      contentsModelMock.findOneAndUpdate = sinon.stub().throws(error);
      try {
        await contentsHelper.editModeResult({reqBody, paramsID, accFromSession});
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });

  describe('.addModeResult()', () => {
    it('should call .addModeResult() correctly', async () => {
      contentsModelMock.create = sinon.stub().resolves(objTest);
      initContentsHelper();
      assert.deepEqual(await contentsHelper.addModeResult({objTest}), objTest);
    });

    it('should call .addModeResult() incorrectly with throw error', async () => {
      const error = new Error('error');
      contentsModelMock.create = sinon.stub().throws(error);
      initContentsHelper();
      try {
        await contentsHelper.addModeResult(objTest);
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });

  describe('.deleteModeResult()', () => {
    it('should call deleteModeResult() correctly', async () => {
      initContentsHelper();
      req.params.id = 0;
      contentsModelMock.deleteOne = sinon.stub().resolves('DONE DELETION');
      assert.deepEqual(await contentsHelper.deleteModeResult({paramsID, accFromSession}), ('DONE DELETION'));
    });

    it('should call .deleteModeResult incorrectly with throw error', async () => {
      initContentsHelper();
      req.params.id = 0;
      const error = new Error('error');
      contentsModelMock.deleteOne = sinon.stub().throws(error);
      try {
        await contentsHelper.deleteModeResult({paramsID, accFromSession});
      } catch (error) {
      }
      assert.deepEqual(error, error);
    });
  });

  describe('.finalResultFromMode()', () => {
    it('should call .finalResultFromMode() correctly', async () => {
      initContentsHelper();
      getObj.accMatched = false;
      assert.deepEqual(await contentsHelper.finalResultFromMode(getObj), {});
    });

    it('should call .finalResultFromMode() correctly with isMatched equal to false', async () => {
      initContentsHelper();
      getObj.accMatched = true;
      contentsHelperMock.viewModeResult = sinon.stub().resolves('MOCK VIEW');
      assert.deepEqual(await contentsHelper.finalResultFromMode(getObj), 'MOCK VIEW');
    });

    it('should call .finalResultFromMode() incorrectly with throw error', async () => {
      initContentsHelper();
      const error = new Error('error');
      contentsHelperMock.viewModeResult = sinon.stub().throws(error);
      contentsModelMock.find = sinon.stub().throws(error);
      contentsHelperMock.finalResultFromMode = sinon.stub().throws(error);
      try {
        await contentsHelper.finalResultFromMode(getObj);
      } catch (error) {
        assert.equal(error, error);
      }
    });
  });
});
