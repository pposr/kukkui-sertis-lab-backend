/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(7) hook.helper.js : pre-save hook mongoose model.', () => {
  let bcryptMock;
  let initHookModel;
  let hookModelMock;
  let methodsStub;
  let thisObj;

  beforeEach(() => {
    thisObj = {
      isModified: sinon.stub()};
    hookModelMock = {
      isModified: sinon.stub()};
    methodsStub = {
      correctPassword: sinon.stub(),
      comparePassword: sinon.stub(),
      isModified: sinon.stub()};
    bcryptMock = {
      hash: sinon.stub(),
      compare: sinon.stub().resolves(true)};

    initHookModel = () => {
      hookModelMock = proxyquire(
          '../../../src/helpers/hook.helper',
          {
            'bcryptjs': bcryptMock,
          });
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.preSaveHook()', () => {
    let next;
    let checkDeepEqual;

    it('should work correctly with true conditions', async () => {
      initHookModel();
      next = function() {
        return true;
      };
      checkDeepEqual = await hookModelMock.preSaveHook(next, thisObj);
      assert.deepEqual(checkDeepEqual, true);
    });

    it('should work correctly with false conditions', async () => {
      initHookModel();
      next = function() {
        return false;
      };
      thisObj.isModified = sinon.stub().resolves(false);
      thisObj.password = 'password';
      thisObj.passwordConfirm = undefined;
      checkDeepEqual = await hookModelMock.preSaveHook(next, thisObj);
      assert.deepEqual(checkDeepEqual, false);
    });
  });
});
