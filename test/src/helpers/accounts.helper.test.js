/* eslint-disable max-len */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(3) authorized.helpers.js : authorized methods for authorized.controller.js', ()=>{
  let accountsMongooseModelMock;
  let generatorMock;
  let initAccountsHelper;
  let accountsHelper;
  let accFromRecord;
  let getObj;
  let query;

  beforeEach(() => {
    accountsMongooseModelMock = {
      findOne: sinon.stub(),
      create: sinon.stub()};
    accountsHelper= {
      checkPassword: sinon.stub(),
      createNewAccount: sinon.stub(),
      setSession: sinon.stub()};
    accFromRecord = {
      username: 'test',
      password: 'test',
      correctPassword: sinon.stub()};
    query = {
      username: 'test',
      password: 'test'};
    generatorMock = {
      generate: sinon.stub()};
    getObj={
      accFromRecord,
      reqBodyUsername: 'test',
      reqBodyPassword: 'test'};

    initAccountsHelper = () => {
      accountsHelper = proxyquire('../../../src/helpers/accounts.helper',
          {'../models/accounts.model': accountsMongooseModelMock,
            'generate-password': generatorMock});
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.checkPassword()', () => {
    it('should work correctly when \'accMatched\' return true', async () => {
      initAccountsHelper();
      accFromRecord.correctPassword = sinon.stub().resolves(true);
      assert.deepEqual(await accountsHelper.checkPassword(getObj), true);
    });

    it('should work correctly when \'accMatched\' return false', async () => {
      initAccountsHelper();
      accFromRecord.correctPassword = sinon.stub().resolves(false);
      assert.deepEqual(await accountsHelper.checkPassword(getObj), false);
    });

    it('should throw error', async () => {
      initAccountsHelper();
      const error = new Error('error');
      accFromRecord.correctPassword = sinon.stub().throws(error);
      try {
        await accountsHelper.checkPassword(getObj);
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });

  describe('.createNewAccount()', () => {
    it('should call createNewAccount with no error', async () => {
      initAccountsHelper();
      generatorMock.generate = sinon.stub().resolves('test');
      accountsMongooseModelMock.create = sinon.stub().resolves(true);
      assert.deepEqual(await accountsHelper.createNewAccount(getObj), query);
    });

    it('should call createNewAccount with an error', async () => {
      initAccountsHelper();
      const error = new Error('error');
      generatorMock.generate = sinon.stub().throws(error);
      accountsMongooseModelMock.create = sinon.stub().throws(error);
      try {
        await accountsHelper.createNewAccount(getObj);
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });
});
