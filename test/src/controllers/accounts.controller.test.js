/* eslint-disable require-jsdoc */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(1) authorized.controller.js : start an account authorization.', () => {
  let accountsMongooseModelMock;
  let resultMock;
  let accountsController;
  let initAccountsController;
  let accountsHelperFunctionMock;
  let schemaResult;
  let req;
  let res;
  let err;

  beforeEach(() => {
    accountsController = {
      authorized: sinon.stub()};
    accountsMongooseModelMock = {
      findOne: sinon.stub(),
      create: sinon.stub()};
    accountsHelperFunctionMock = {
      checkPassword: sinon.stub(),
      createNewAccount: sinon.stub(),
      setSession: sinon.stub()};
    req = {
      session: {username: 'test', password: 'test'},
      body: {username: 'test', password: 'test'},
      params: {id: {}},
    };
    res = {
      json(resultMock) {
        return resultMock;
      },
      status(responseStatus) {
        return this;
      }};
    schemaResult = {
      username: 'test',
      password: 'test'};
    err = null;

    initAccountsController = () => {
      accountsController = proxyquire(
          '../../../src/controllers/accounts.controller',
          {
            '../models/accounts.model': accountsMongooseModelMock,
            '../helpers/accounts.helper': accountsHelperFunctionMock,
          });
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.authorized() ', () => {
    it('should call .authorized() method for type checked', async () => {
      initAccountsController();
      assert.deepEqual(typeof accountsController.authorized, 'function');
    });

    it('should call .authorized() correctly with foundUser equal to true', async () => {
      initAccountsController();
      accountsMongooseModelMock.findOne = sinon.stub().resolves(schemaResult);
      accountsHelperFunctionMock.checkPassword = sinon.stub().resolves(true);
      assert.deepEqual(await accountsController.authorized(req, res, err), ({'AUTHORIZATION STATUS': true}));
    });

    it('should call .authorized() correctly with foundUser equal to false', async () => {
      initAccountsController();
      resultMock = ({username: 'newUsername', password: 'newPassword'});
      accountsMongooseModelMock.findOne = sinon.stub().resolves(false);
      accountsHelperFunctionMock.createNewAccount = sinon.stub().resolves(resultMock);
      assert.deepEqual(await accountsController.authorized(req, res, err), {'NEW ACCOUNT GENERATED': resultMock});
    });

    it('should call .authorized() incorrectly with throw error', async () => {
      initAccountsController();
      const error = new Error('error');
      accountsMongooseModelMock.findOne = sinon.stub().throws(error);
      assert.deepEqual(await accountsController.authorized(req, res, err), {error});
    });
  });
});
