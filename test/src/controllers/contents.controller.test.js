/* eslint-disable camelcase */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(2) contents.controller.js : control the blogposts on CRUD processes.', () => {
  let accountsMongooseModelMock;
  let postContentsObj;
  let contentsMongooseModelMock;
  let contentsController;
  let allPostsResult;
  let showResult;
  let user;
  let sendObj;
  let req;
  let res;
  let next;
  let post;
  let jsonStub;
  let initContentsController;
  let contentsHelperFunctionMock;
  //
  beforeEach(() => {
    post = 'res.send.done';
    showResult = {
      _id: 1,
      username: 'test',
      password: 'test',
      __v: 0};
    contentsController = {
      myPosts: sinon.stub(),
      postContents: sinon.stub(),
      allPosts: sinon.stub()};
    accountsMongooseModelMock = {
      findOne: sinon.stub()};
    contentsMongooseModelMock = {
      create: sinon.stub(),
      find: sinon.stub(),
      findOneAndUpdate: sinon.stub(),
      deleteOne: sinon.stub()};
    contentsHelperFunctionMock = {
      myPosts: sinon.stub(),
      postContents: sinon.stub(),
      addPosts: sinon.stub(),
      allPosts: sinon.stub(),
      checkSessionExist: sinon.stub(),
      usernameCheck: sinon.stub().returns(true),
      viewModeResult: sinon.stub(),
      editModeResult: sinon.stub(),
      addModeResult: sinon.stub(),
      deleteModeResult: sinon.stub(),
      finalResultFromMode: sinon.stub().returns(showResult)};
    jsonStub = {message: 'json_test'},
    user = {
      comparePassword: sinon.stub()};
    req = {
      session: {username: 'test', password: 'test'},
      body: {username: 'test', password: 'test'},
      params: {id: {}},
    };
    res = {
      json(resultMock) {
        return resultMock;
      },
      status(responseStatus) {
        return this;
      },
    };
    sendObj = {
      mode: {},
      accFromSession: req.session,
      reqBodyUsername: req.body.username,
      reqBodyPassword: req.body.password,
      reqBody: req.body,
      paramsID: req.params.id,
    };
    contentsController = {
      myPosts: sinon.stub(),
      addPosts: sinon.stub(),
      allPosts: sinon.stub(),
      editPosts: sinon.stub(),
      deletePosts: sinon.stub(),
      postContents: {},
    };
    allPostsResult = {
      _id: 0,
      username: 'test',
      content: 'test',
      cardName: 'test',
      cardStatus: 'test',
      cardContent: 'test',
      cardCategory: 'test',
      __v: 0,
    };
    postContentsObj = {
      mode: {},
      accFromSession: {username: 'test', password: 'test'},
      reqBodyUsername: 'test',
      reqBodyPassword: 'test',
      reqBody: {username: 'test', password: 'test'},
      paramsID: req.params.id,
      accFromRecord: {username: 'test', password: 'test'},
      accMatched: {},
    };
    next = sinon.stub();

    initContentsController = () => {
      contentsController = proxyquire('../../../src/controllers/contents.controller', {
        '../models/accounts.model': accountsMongooseModelMock,
        '../models/contents.model': contentsMongooseModelMock,
        '../helpers/contents.helper': contentsHelperFunctionMock,
      });
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.myPosts()', () => {
    it('should call .myPosts() correctly', async () => {
      initContentsController();
      sendObj.mode = 'view';
      req.params.id = null;
      contentsController.postContents = sinon.stub().resolves(sendObj);
      assert.deepEqual(await contentsController.myPosts(req, res, next), {'MY POSTS': sendObj});
    });

    it('should call .myPosts() incorrectly with .checkSessionExist throw error', async () => {
      initContentsController();
      const error = new Error('error');
      contentsController.postContents = sinon.stub().throws(error);
      assert.deepEqual(await contentsController.myPosts(req, res, next), {error});
    });
  });

  describe('.addPosts()', () => {
    it(('should call .addPosts() correctly'), async () => {
      initContentsController();
      sendObj.mode = 'add';
      req.params.id = null;
      contentsController.postContents = sinon.stub().resolves(sendObj);
      assert.deepEqual(await contentsController.addPosts(req, res, next), {'COMPLETED ADD': sendObj});
    });

    it('should call .addPosts() incorrectly with .postContents throw error', async () => {
      initContentsController();
      const error = new Error('error');
      contentsController.postContents = sinon.stub().throws(error);
      assert.deepEqual(await contentsController.addPosts(req, res, next), {error});
    });
  });

  describe('.allPosts()', () => {
    it(('should call allPosts() correctly'), async () => {
      initContentsController();
      contentsMongooseModelMock.find.resolves(allPostsResult);
      assert.deepEqual(await contentsController.allPosts(req, res, next), {'ALL POSTS': allPostsResult});
    });

    it('should call .allposts() incorrectly with .find() throw error', async () => {
      initContentsController();
      const error = new Error('error');
      contentsMongooseModelMock.find.throws(error);
      assert.deepEqual(await contentsController.allPosts(req, res, next), {error});
    });
  });

  describe('.editposts()', () => {
    it(('should call .editposts() correctly'), async () => {
      initContentsController();
      sendObj.mode = 'edit';
      contentsController.postContents = sinon.stub().resolves(req.body);
      assert.deepEqual(await contentsController.editPosts(req, res, next), {'COMPLETED EDIT': req.body});
    });

    it('should call .editposts() incorrectly with all methods throw error', async () => {
      initContentsController();
      req.params.id = 0;
      const error = new Error('error');
      contentsHelperFunctionMock.checkSessionExist.throws(error);
      accountsMongooseModelMock.findOne.throws(error);
      contentsHelperFunctionMock.usernameCheck.throws(error);
      contentsHelperFunctionMock.finalResultFromMode.throws(error);
      contentsController.postContents = sinon.stub().throws(error);
      assert.deepEqual(await contentsController.editPosts(req, res, next), {error});
    });
  });

  describe('.deleteposts()', () => {
    it(('should call .deleteposts() correctly'), async () => {
      initContentsController();
      sendObj.mode = 'delete';
      req.params.id = 0;
      contentsController.postContents = sinon.stub().resolves(sendObj);
      assert.deepEqual(await contentsController.deletePosts(req, res, next), {'COMPLETED DELETE': sendObj});
    });

    it('should call .deleteposts() incorrectly with throw error', async () => {
      initContentsController();
      const error = new Error('error');
      contentsController.postContents = sinon.stub().throws(error);
      assert.deepEqual(await contentsController.deletePosts(req, res, next), {error});
    });
  });

  describe('.postContents()', () => {
    it('should call .postContents() correctly', async () => {
      initContentsController();
      contentsHelperFunctionMock.checkSessionExist.resolves({username: 'test', password: 'test'});
      accountsMongooseModelMock.findOne.resolves({username: 'test', password: 'test'});
      contentsHelperFunctionMock.usernameCheck.resolves(true);
      contentsHelperFunctionMock.finalResultFromMode.resolves(sendObj);
      assert.deepEqual(await contentsController.postContents(sendObj), sendObj);
    });

    it('should call .postContents() correctly with non matched', async () => {
      initContentsController();
      postContentsObj.mode = 'view';
      req.params.id = 0;
      postContentsObj.accMatched = false;
      contentsHelperFunctionMock.checkSessionExist.resolves({username: 'test', password: 'test'});
      accountsMongooseModelMock.findOne.resolves({username: 'test', password: 'test'});
      contentsHelperFunctionMock.usernameCheck.resolves(false);
      contentsHelperFunctionMock.finalResultFromMode.resolves(postContentsObj);
      assert.deepEqual(await contentsController.postContents(postContentsObj), []);
    });

    it('should call .postContents() incorrectly with throw error', async () => {
      initContentsController();
      const error = new Error('error');
      contentsHelperFunctionMock.finalResultFromMode = sinon.stub().throws(error);
      try {
        await contentsController.postContents(postContentsObj);
      } catch (error) {
        assert.deepEqual(error, error);
      }
    });
  });
});
