/* eslint-disable new-cap */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(6) contents.model.js: mongoose Schema and Configuration for \'user\' model.', () => {
  let bcryptMock;
  let mongooseSchema;
  let initContentsModel;
  let pre;
  let blogModelMock;
  let next;
  let mongooseMock;
  let contentsModel;
  let methodsStub;
  let preStub;
  let preSave;
  let nextFunction;
  let obj;
  let authMod;
  let authModel;
  let hookMongooseModelMock;
  let preSaveStub;
  let req;

  beforeEach(() => {
    authModel = {
      pre: sinon.stub()};
    obj = {
      isModified: sinon.stub()};
    hookMongooseModelMock = {
      preSaveHook: sinon.stub().resolves(true)};
    methodsStub = {
      correctPassword: sinon.stub(),
      comparePassword: sinon.stub()};
    preSaveStub = {
      isModified: sinon.stub()};
    preStub = sinon.stub(),
    mongooseMock = {
      model: sinon.stub(),
      Schema: sinon.stub().returns({
        pre: sinon.stub(),
        methods: methodsStub})};
    bcryptMock = {
      hash: sinon.stub().resolves('test'),
      compare: sinon.stub().resolves(true)};
    req = {
      session: {username: 'test', password: 'test'},
      body: {username: 'test', password: 'test'},
      params: {id: {}},
    };
    blogModelMock = {pre: sinon.stub()};

    initContentsModel = () => {
      contentsModel = proxyquire(
          '../../../src/models/contents.model',
          {
            'mongoose': mongooseMock,
            'bcryptjs': bcryptMock,
            '../helpers/hook.helper': hookMongooseModelMock,
          });
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.correctPassword()', () => {
    let typedPassword;
    let originalPassword;

    it('should work correctly with comparePassword comparison', async () => {
      initContentsModel();
      typedPassword = 'password';
      originalPassword = 'password';
      bcryptMock.compare = sinon.stub().resolves(true);
      assert.deepEqual(await methodsStub.correctPassword(typedPassword, originalPassword), true);
      assert.deepEqual(bcryptMock.compare.args[0][0], typedPassword);
      assert.deepEqual(bcryptMock.compare.args[0][1], originalPassword);
    });
  });

  describe('.pre(save, func)', () => {
    let contentsMongooseModelMock;
    let newPosts;

    it('should work correctly with preSaveHook functions', async () => {
      initContentsModel();
      contentsMongooseModelMock = require('../../../src/models/contents.model');
      newPosts = new contentsMongooseModelMock({
        pre: sinon.stub(),
        Schema: sinon.stub().returns(nextFunction),
      });
      nextFunction = {
        username: String,
        password: String,
        content: String,
        cardName: String,
        cardStatus: String,
        cardContent: String,
        cardCategory: String,
        pre: sinon.stub().returns(true),
      };
      newPosts.save();
      nextFunction.pre.returns(true);
      bcryptMock.compare.resolves(true);
      assert.deepEqual(await nextFunction.pre('save', next), true);
    });
  });
});
