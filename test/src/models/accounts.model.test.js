/* eslint-disable new-cap */
/* eslint-disable max-len */
/* eslint-disable require-jsdoc */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
'use strict';

const assert = require('chai').assert;
const proxyquire = require('proxyquire').noCallThru();
const sinon = require('sinon');

describe('(5) authorized.model.js : mongoose Schema and Configuration for \'authorized\' model.', () => {
  let bcryptMock;
  let mongooseSchema;
  let initAccountsModel;
  let pre;
  let callNext;
  let hookMongooseModelMock;
  let mongooseMock;
  let accountsModel;
  let accountsModelMock;
  let authMod;
  let methodsStub;
  let obj;
  let preSave;
  let preStub;
  let preSaveStub;
  let next;
  let Schema;

  beforeEach(() => {
    accountsModel = {
      pre: sinon.stub(),
    };
    hookMongooseModelMock = {
      preSaveHook: sinon.stub().resolves(true),
    };
    methodsStub = {
      correctPassword: sinon.stub(),
      comparePassword: sinon.stub(),
    };
    preSaveStub = {
      isModified: sinon.stub(),
    };
    mongooseMock = {
      model: sinon.stub(),
      Schema: sinon.stub().returns({
        pre: sinon.stub(),
        methods: methodsStub,
        preSave: preSaveStub,
      }),
    };
    bcryptMock = {
      hash: sinon.stub().resolves('test'),
      compare: sinon.stub().resolves(true),
    };

    initAccountsModel = () => {
      accountsModel = proxyquire(
          '../../../src/models/accounts.model',
          {
            'mongoose': mongooseMock,
            'bcryptjs': bcryptMock,
            '../src/helpers/hook.helper': hookMongooseModelMock,
          });
    };
  });

  afterEach(() => {
    sinon.restore();
  });

  describe('.correctPassword()', () => {
    let typedPassword;
    let originalPassword;

    it('should work correctly with comparePassword comparison', async () => {
      initAccountsModel();
      typedPassword = 'password';
      originalPassword = 'password';
      bcryptMock.compare = sinon.stub().resolves(true);
      assert.deepEqual(await methodsStub.correctPassword(typedPassword, originalPassword), true);
      assert.deepEqual(bcryptMock.compare.args[0][0], typedPassword);
      assert.deepEqual(bcryptMock.compare.args[0][1], originalPassword);
    });
  });

  describe('.pre(save, func)', () => {
    let authModelMock;
    let newAccount;

    it('should work correctly', async () => {
      initAccountsModel();
      authModelMock = require('../../../src/models/accounts.model');
      newAccount = new authModelMock({
        username: 'test',
        password: 'test',
        pre: sinon.stub(),
        Schema: sinon.stub().returns(callNext),
      });
      callNext = {
        username: String,
        password: String,
        pre: sinon.stub().returns(true),
      };
      newAccount.save();
      callNext.pre = sinon.stub().returns(true);
      bcryptMock.compare = sinon.stub().resolves(true);
      assert.deepEqual(await callNext.pre('save', next), true);
    });
  });
});
