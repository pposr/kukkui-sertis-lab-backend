# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
## [Released]
## [5.0.0] – 2020-01-06

### Added

* All threw error was handled now in the same way. With the new responses back as [res.status(500)](#).
* All [res](#) contents response with [JSON](#) format. Even though the empty responses.
* New [res,status,json](#) was added to the unit tests for more corresponding to [res.status.json](#).

### Changed

* Response back with a status code 200,201,500 instead of [res.send](#).
* Some bad [returns error](#) was changed into all standardized way like [throw error](#).
* Rename all functions inside [src](#) and [test](#) folder with a camelCase concepts and try to be verb phrases if possible.
* Rename every variables inside [src](#) and [test](#) folder to be more meaningful and try to be noun phrases if possible.
* All names now changed to be more meaningful and easy to understand what they used for. 
* [query](#) object was set and used. Instead of directly sent one long mongoose query inone line.
* All mock-variables and mock-functions was also refactor to be matched with the name in the src files.
